package test.anshul.com;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import test.anshul.com.di.AppComponent;
import test.anshul.com.di.DaggerAppComponent;
import test.anshul.com.di.UtilsModule;

public class MyApplication extends Application {

    public static  MyApplication myApplication;

    public static MyApplication getInstance(){
       return  myApplication ;

    }

    AppComponent appComponent;
    Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        myApplication = this;
        appComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).utilsModule(new UtilsModule()).build();
        Log.e("anshul","component is " + appComponent);
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
    }
}
