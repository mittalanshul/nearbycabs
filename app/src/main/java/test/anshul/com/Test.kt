package test.anshul.com;

class Test  {
    // property (data member)
    private var name: String = "Tutorials.point"

    // member function
    fun printMe(x:String) : String{
        return x;
    }
}
fun main(args: Array<String>) {
    val obj = Test() // create obj object of myClass class

    System.out.println("You are at the best Learning website Named-  "+ obj.printMe("4"))
}

