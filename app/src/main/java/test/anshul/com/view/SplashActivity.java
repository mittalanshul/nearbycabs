package test.anshul.com.view;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import test.anshul.com.R;
import test.anshul.com.utils.NetworkUtils;

public class SplashActivity extends AppCompatActivity {
    private Handler mWaitHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(NetworkUtils.getConnectivityStatus(this) != 0){
            mWaitHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        launchMaps();
                        finish();
                    } catch (Exception ignored) {
                        ignored.printStackTrace();
                    }
                }
            }, 2000);  // Give a 5 seconds delay.
        } else{
            Toast.makeText(this,"Please check your network connection",Toast.LENGTH_SHORT).show();
        }
    }

    public void launchMaps(){
        Intent intent = new Intent(this,CabsMapsActivity.class);
        startActivity(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mWaitHandler.removeCallbacksAndMessages(null);
    }
}
