package test.anshul.com.view;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.lang.reflect.Field;
import java.util.ArrayList;

import butterknife.BindView;
import test.anshul.com.R;
import test.anshul.com.adapter.CarListAdapter;
import test.anshul.com.model.CarList;
import test.anshul.com.model.PlaceMarks;

public class CarListActivity extends AppCompatActivity {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    CarList carList;
    ArrayList<PlaceMarks> placeMarksArrayList;

    Toolbar toolbar;

    private SearchView mSearchView;
    CarListAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_list);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("Cars List");
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        if (getIntent() != null && getIntent().getExtras() != null) {
            carList = getIntent().getParcelableExtra("car_list");
            placeMarksArrayList = carList.placemarks;
            adapter = new CarListAdapter(placeMarksArrayList);
            recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(adapter);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
                finish();
            }
        return super.onOptionsItemSelected(item);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.search_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchManager searchManager = (SearchManager) this.getSystemService(Context.SEARCH_SERVICE);
        if (searchItem != null) {
            mSearchView = (SearchView) searchItem.getActionView();
        }
        if (mSearchView != null) {
            mSearchView.setSearchableInfo(searchManager.getSearchableInfo(this.getComponentName()));
            EditText etSearch = ((EditText) mSearchView.findViewById(android.support.v7.appcompat.R.id.search_src_text));
            try {
                Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
                f.setAccessible(true);
                f.set(etSearch, R.drawable.cursor);// set textCursorDrawable to null
            } catch (Exception e) {
                e.printStackTrace();
            }
            etSearch.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
            mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String query) {
                    Log.i("anshul","onQueryTextChange " + query);
                        if (!TextUtils.isEmpty(query)) {
                           refreshDataOnSearch(query);
                        } else {
                           loadDefaultData();
                        }
                    return false;
                }
            });
        }
        return super.onCreateOptionsMenu(menu);
    }

    public void loadDefaultData(){
        if(placeMarksArrayList != null && placeMarksArrayList.size() >0){
            if(adapter != null){
                recyclerView.setVisibility(View.VISIBLE);
                adapter.setPlaceMarksArrayList(placeMarksArrayList);
                adapter.notifyDataSetChanged();
            }
        } else{
            recyclerView.setVisibility(View.GONE);
        }
    }

    public void refreshDataOnSearch(String name){
        ArrayList<PlaceMarks> queriedList = queriedStaffList(name);
        if(queriedList != null && queriedList.size() >0){
            if(adapter != null){
                recyclerView.setVisibility(View.VISIBLE);
                adapter.setPlaceMarksArrayList(queriedList);
                adapter.notifyDataSetChanged();
            }
        } else{
            recyclerView.setVisibility(View.GONE);
        }
    }

    private ArrayList<PlaceMarks> queriedStaffList(String query){
        ArrayList<PlaceMarks> placeMarksArrayList = new ArrayList<>();
        for(PlaceMarks placeMarks : this.placeMarksArrayList){
            if(placeMarks.address.toLowerCase().contains(query.toLowerCase())){
                placeMarksArrayList.add(placeMarks);
            }
            if(placeMarks.engineType.toLowerCase().contains(query.toLowerCase())){
                placeMarksArrayList.add(placeMarks);
            }
            if(placeMarks.exterior.toLowerCase().contains(query.toLowerCase())){
                placeMarksArrayList.add(placeMarks);
            }
            if(placeMarks.fuel.toLowerCase().contains(query.toLowerCase())){
                placeMarksArrayList.add(placeMarks);
            }
            if(placeMarks.interior.toLowerCase().contains(query.toLowerCase())){
                placeMarksArrayList.add(placeMarks);
            }
            if(placeMarks.vin.toLowerCase().contains(query.toLowerCase())){
                placeMarksArrayList.add(placeMarks);
            }
        }
        return placeMarksArrayList;
    }

}
