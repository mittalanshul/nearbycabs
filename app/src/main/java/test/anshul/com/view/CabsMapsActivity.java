package test.anshul.com.view;

import android.Manifest;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.annotations.Nullable;
import test.anshul.com.MyApplication;
import test.anshul.com.R;
import test.anshul.com.ViewModelFactory;
import test.anshul.com.adapter.MarkerInfoWindowAdapter;
import test.anshul.com.model.ApiResponse;
import test.anshul.com.model.CarList;
import test.anshul.com.model.PlaceMarks;
import test.anshul.com.model.Status;
import test.anshul.com.utils.PermissionUtils;
import test.anshul.com.viewmodel.CabsViewModel;

public class CabsMapsActivity extends AppCompatActivity implements
        OnMapReadyCallback, ActivityCompat.OnRequestPermissionsResultCallback, GoogleMap.OnMarkerClickListener {

    @Inject
    ViewModelFactory viewModelFactory;
    CabsViewModel viewModel;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    private boolean mPermissionDenied = false;
    private GoogleMap mMap;
    Marker mCurrLocationMarker;
    CarList carList;

    Toolbar toolbar;
    MarkerOptions currentLocMarkerOptions;
    boolean isMarkerAlreadyClicked;
    ArrayList<MarkerOptions> markerOptionsArrayList;
    private double wayLatitude = 0.0, wayLongitude = 0.0;
    private FusedLocationProviderClient mFusedLocationClient;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("M-Cabs");
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        ButterKnife.bind(this);

        MyApplication.getInstance().getAppComponent().doInjection(this);

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(CabsViewModel.class);

        viewModel.loginResponse().observe(this, new Observer<ApiResponse>() {
            @Override
            public void onChanged(@Nullable ApiResponse apiResponse) {
                consumeResponse(apiResponse);
            }
        });

        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    LOCATION_PERMISSION_REQUEST_CODE);

        } else {
            setLocationListener();
        }

    }

    public void setLocationListener() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mFusedLocationClient.getLastLocation().addOnSuccessListener(this, location -> {
            if (location != null) {
                wayLatitude = location.getLatitude();
                wayLongitude = location.getLongitude();
                if (mCurrLocationMarker != null) {
                    mCurrLocationMarker.remove();
                }

                LatLng latLng = new LatLng(wayLatitude, wayLongitude);
                currentLocMarkerOptions = new MarkerOptions();
                currentLocMarkerOptions.position(latLng);
                currentLocMarkerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
                mCurrLocationMarker = mMap.addMarker(currentLocMarkerOptions);
                mCurrLocationMarker.setTag("Current");

                //move map camera
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 13));
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(new LatLng(wayLatitude, wayLongitude))      // Sets the center of the map to location user
                        .zoom(13)                   // Sets the zoom
                        .tilt(40)                   // Sets the tilt of the camera to 30 degrees
                        .build();                   // Creates a CameraPosition from the builder
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                viewModel.getCabsApi();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.login)
    void onLoginClicked() {

    }

    /*
     * method to validate $(mobile number) and $(password)
     * */
    private boolean isValid() {
        return true;
    }

    /*
     * method to handle response
     * */
    private void consumeResponse(ApiResponse apiResponse) {

        Log.i("anshul", "the data is " + apiResponse.status);
        switch (apiResponse.status) {

            case LOADING:
                //progressDialog.show();
                break;

            case SUCCESS:
                //progressDialog.dismiss();
                renderSuccessResponse((CarList) apiResponse.data);
                ApiResponse apiResponse1 = new ApiResponse(Status.SUCCESS, null, null);
                viewModel.loginResponse().setValue(apiResponse1);
                break;

            case ERROR:
                //progressDialog.dismiss();
                Toast.makeText(this, apiResponse.error.getMessage(), Toast.LENGTH_SHORT).show();
                break;

            default:
                break;
        }
    }

    /*
     * method to handle success response
     * */
    private void renderSuccessResponse(CarList response) {
        Log.i("anshul", response.placemarks.size() + "");
        showAllCarsOnMap(response);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.setMaxZoomPreference(11);
        mMap.setOnMarkerClickListener(this);
        MarkerInfoWindowAdapter markerInfoWindowAdapter = new MarkerInfoWindowAdapter(getApplicationContext());
        googleMap.setInfoWindowAdapter(markerInfoWindowAdapter);
       // enableMyLocation();
    }

    /**
     * Enables the My Location layer if the fine location permission has been granted.
     */
    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        } else if (mMap != null) {
            mMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case LOCATION_PERMISSION_REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                            && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    setLocationListener();
                } else {
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
                }
                break;
            }
        }
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        if (mPermissionDenied) {
            showMissingPermissionError();
            mPermissionDenied = false;
        }
    }

    /**
     * Displays a dialog with error message explaining that the location permission is missing.
     */
    private void showMissingPermissionError() {
        PermissionUtils.PermissionDeniedDialog
                .newInstance(true).show(getSupportFragmentManager(), "dialog");
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    public void showAllCarsOnMap(CarList carList){
        this.carList = carList;

        if(carList != null && carList.placemarks != null && carList.placemarks.size() >0){
            markerOptionsArrayList = new ArrayList<>();
            for(PlaceMarks placeMarks : carList.placemarks){
                ArrayList<String> coordinates = placeMarks.coordinates;
                LatLng latLng = new LatLng(Double.valueOf(coordinates.get(1)), Double.valueOf(coordinates.get(0)));
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(latLng);
                markerOptions.title(placeMarks.name);
                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                markerOptionsArrayList.add(markerOptions);
                mMap.addMarker(markerOptions);
            }
        }
    }

    @OnClick(R.id.login)
    public void onLoginClicked(View view) {
        if(carList != null){
            Intent intent = new Intent(this, CarListActivity.class);
            intent.putExtra("car_list",this.carList);
            startActivity(intent);
        } else {
            Toast.makeText(this,"Please wait .. Getting cars near you",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {

        if(marker.getTag() != null && ((String)marker.getTag()).equalsIgnoreCase("Current")){
            Toast.makeText(this,((String)marker.getTag()),Toast.LENGTH_SHORT).show();
        } else{
            Toast.makeText(this,marker.getTitle(),Toast.LENGTH_SHORT).show();
            mMap.clear();
            if(!isMarkerAlreadyClicked){
                disableAllMarkers(marker);
            }else{

                enableAllMarkers();
            }
        }


        return false;
    }

    /**
     * This method is used to enable all the markers
     */

    public void enableAllMarkers(){
        isMarkerAlreadyClicked = false;
        mMap.addMarker(currentLocMarkerOptions);
        if(markerOptionsArrayList != null && markerOptionsArrayList.size() >0){
            for(MarkerOptions markerOptions : markerOptionsArrayList){
                mMap.addMarker(markerOptions).hideInfoWindow();
            }
        }
    }

    public void disableAllMarkers(Marker marker){
        isMarkerAlreadyClicked = true;
        mMap.clear();
        mMap.addMarker(currentLocMarkerOptions);
        MarkerInfoWindowAdapter markerInfoWindowAdapter = new MarkerInfoWindowAdapter(getApplicationContext());
        mMap.setInfoWindowAdapter(markerInfoWindowAdapter);
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(marker.getPosition());
        markerOptions.title(marker.getTitle());
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        mMap.addMarker(markerOptions).showInfoWindow();
    }
}
