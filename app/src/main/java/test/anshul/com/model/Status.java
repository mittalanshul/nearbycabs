package test.anshul.com.model;

public enum Status {
    LOADING,
    SUCCESS,
    ERROR,
    COMPLETED
}