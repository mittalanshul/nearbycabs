package test.anshul.com.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class PlaceMarks implements Parcelable {

    public String address;
    public ArrayList<String> coordinates;
    public String engineType;
    public String exterior;
    public String fuel;
    public String interior;
    public String vin;
    public String name;

    protected PlaceMarks(Parcel in) {
        address = in.readString();
        coordinates = in.createStringArrayList();
        engineType = in.readString();
        exterior = in.readString();
        fuel = in.readString();
        interior = in.readString();
        vin = in.readString();
        name = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(address);
        dest.writeStringList(coordinates);
        dest.writeString(engineType);
        dest.writeString(exterior);
        dest.writeString(fuel);
        dest.writeString(interior);
        dest.writeString(vin);
        dest.writeString(name);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PlaceMarks> CREATOR = new Creator<PlaceMarks>() {
        @Override
        public PlaceMarks createFromParcel(Parcel in) {
            return new PlaceMarks(in);
        }

        @Override
        public PlaceMarks[] newArray(int size) {
            return new PlaceMarks[size];
        }
    };
}
