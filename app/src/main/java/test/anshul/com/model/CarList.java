package test.anshul.com.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class CarList implements Parcelable {

    public ArrayList<PlaceMarks> placemarks;


    protected CarList(Parcel in) {
        placemarks = in.createTypedArrayList(PlaceMarks.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(placemarks);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CarList> CREATOR = new Creator<CarList>() {
        @Override
        public CarList createFromParcel(Parcel in) {
            return new CarList(in);
        }

        @Override
        public CarList[] newArray(int size) {
            return new CarList[size];
        }
    };
}
