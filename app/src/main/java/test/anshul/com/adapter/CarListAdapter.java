package test.anshul.com.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import test.anshul.com.R;
import test.anshul.com.model.PlaceMarks;

public class CarListAdapter extends RecyclerView.Adapter<CarListAdapter.ViewHolder>{
    public ArrayList<PlaceMarks> getPlaceMarksArrayList() {
        return placeMarksArrayList;
    }

    public void setPlaceMarksArrayList(ArrayList<PlaceMarks> placeMarksArrayList) {
        this.placeMarksArrayList = placeMarksArrayList;
    }

    private ArrayList<PlaceMarks> placeMarksArrayList;

    // RecyclerView recyclerView;
    public CarListAdapter(ArrayList<PlaceMarks> listdata) {
        this.placeMarksArrayList = listdata;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final PlaceMarks myListData = placeMarksArrayList.get(position);
        holder.textAddress.setText(placeMarksArrayList.get(position).address);
        holder.textEngine.setText(placeMarksArrayList.get(position).engineType);
        holder.textExt.setText(placeMarksArrayList.get(position).exterior);
        holder.textFuel.setText(placeMarksArrayList.get(position).fuel);
        holder.textInt.setText(placeMarksArrayList.get(position).interior);
        holder.textVin.setText(placeMarksArrayList.get(position).vin);
        holder.textName.setText(placeMarksArrayList.get(position).name);
    }


    @Override
    public int getItemCount() {
        return placeMarksArrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public TextView textAddress;
        public TextView textEngine;
        public TextView textExt;
        public TextView textFuel;
        public TextView textInt;
        public TextView textVin;
        public TextView textName;
        public RelativeLayout relativeLayout;
        public ViewHolder(View itemView) {
            super(itemView);
            this.imageView = (ImageView) itemView.findViewById(R.id.imageView);
            this.textAddress = (TextView) itemView.findViewById(R.id.textAddress);
            this.textEngine = (TextView) itemView.findViewById(R.id.textEngine);
            this.textExt = (TextView) itemView.findViewById(R.id.textExt);
            this.textFuel = (TextView) itemView.findViewById(R.id.textFuel);
            this.textInt = (TextView) itemView.findViewById(R.id.textInt);
            this.textVin = (TextView) itemView.findViewById(R.id.textVin);
            this.textName = (TextView) itemView.findViewById(R.id.textName);
            relativeLayout = (RelativeLayout)itemView.findViewById(R.id.relativeLayout);
        }
    }
}
