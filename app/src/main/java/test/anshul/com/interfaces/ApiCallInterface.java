package test.anshul.com.interfaces;

import com.google.gson.JsonElement;

import io.reactivex.Observable;
import retrofit2.http.GET;
import test.anshul.com.Urls;

public interface ApiCallInterface {

    @GET(Urls.LOGIN)
    Observable<JsonElement> getCabs();
}
