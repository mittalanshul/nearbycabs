package test.anshul.com.repository;

import com.google.gson.JsonElement;

import io.reactivex.Observable;
import test.anshul.com.interfaces.ApiCallInterface;

public class Repository {
    private ApiCallInterface apiCallInterface;

    public Repository(ApiCallInterface apiCallInterface) {
        this.apiCallInterface = apiCallInterface;
    }

    /*
     * method to call login api
     * */
    public Observable<JsonElement> executeGetCabs() {
        return apiCallInterface.getCabs();
    }


}
