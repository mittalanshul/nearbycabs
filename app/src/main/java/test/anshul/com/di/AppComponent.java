package test.anshul.com.di;

import javax.inject.Singleton;

import dagger.Component;
import test.anshul.com.AppModule;
import test.anshul.com.view.CabsMapsActivity;

@Component(modules = {AppModule.class, UtilsModule.class})
@Singleton
public interface AppComponent {

    void doInjection(CabsMapsActivity cabsMapsActivity);

}
