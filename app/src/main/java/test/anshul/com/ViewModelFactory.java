package test.anshul.com;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import test.anshul.com.repository.Repository;
import test.anshul.com.viewmodel.CabsViewModel;

public class ViewModelFactory implements ViewModelProvider.Factory {

    private Repository repository;

    @Inject
    public ViewModelFactory(Repository repository) {
        this.repository = repository;
    }


    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(CabsViewModel.class)) {
            return (T) new CabsViewModel(repository);
        }
        throw new IllegalArgumentException("Unknown class name");
    }

}
