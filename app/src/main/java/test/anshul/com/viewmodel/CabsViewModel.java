package test.anshul.com.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import test.anshul.com.model.ApiResponse;
import test.anshul.com.model.CarList;
import test.anshul.com.repository.Repository;

public class CabsViewModel extends ViewModel {

    private Repository repository;
    private final CompositeDisposable disposables = new CompositeDisposable();
    private final MutableLiveData<ApiResponse> responseLiveData = new MutableLiveData<>();


    public CabsViewModel(Repository repository) {
        this.repository = repository;
    }

    public MutableLiveData<ApiResponse> loginResponse() {
        return responseLiveData;
    }

    /*
     * method to call normal login api with $(mobileNumber + password)
     * */
    public void getCabsApi() {

        disposables.add(repository.executeGetCabs()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe((d) -> responseLiveData.setValue(ApiResponse.loading()))
                .map(new Function<JsonElement, CarList>() {
                    @Override
                    public CarList apply(JsonElement jsonElement) throws Exception {
                        return new Gson().fromJson(jsonElement,CarList.class);
                    }
                })
                .subscribe(
                        result -> responseLiveData.setValue(ApiResponse.success(result)),
                        throwable -> responseLiveData.setValue(ApiResponse.error(throwable))
                ));
    }



    @Override
    protected void onCleared() {
        disposables.clear();
    }

    public void disableLogin(){
    }
}
